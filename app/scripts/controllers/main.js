'use strict';

/**
 * @ngdoc function
 * @name subbuteoMatchesApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the subbuteoMatchesApp
 */
angular.module('subbuteoMatchesApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
