'use strict';

/**
 * @ngdoc function
 * @name subbuteoMatchesApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the subbuteoMatchesApp
 */
angular.module('subbuteoMatchesApp')
  .controller('NuevoTorneoCtrl', ['$scope', function ($scope) {
    //función al hacer click en siguiente al comenzar un nuevo torneo
    $scope.numeroEquipos = 4;
    $scope.nuevoCampeonato = true;
    $scope.elegirEquipos = false;
    $scope.crearEmparejamientos = false;
    $scope.controlNumeroEquipos = function() {
      $scope.numero = numeroEquipos.value;
      if($scope.numero < 4) {
        numeroEquipos.value = 4;
      }
      if($scope.numero > 16) {
        numeroEquipos.value = 16;
      }
      if($scope.numero % 2 === 1) {
        numeroEquipos.value = parseInt($scope.numero) + 1;
      }
    };
    $scope.btnNumeroEquipos = function() {
      $scope.numeroequipos = parseInt(numeroEquipos.value);
      $scope.nuevoCampeonato = false;
      $scope.elegirEquipos = true;
      /* Creamos el scope en el que aparecerán el número de equipos */
      $scope.equipos = [];
      /* Vamos montando input por cada uno de los equipos */
      for (var index = 1; index <= $scope.numeroequipos; index++) {
        $scope.equipos.push({id: index, name: "Equipo" + index});
      }
    };

    $scope.btnCrearEmparejamientos = function() {
      /**
       * AQUÍ HAY QUE REALIZAR LA VALIDACIÓN DE QUE TODO ES CORRECTO
       */
      // Ocultamos la fase de elegir equipos y mostramos la de crear emparejamientos
      $scope.elegirEquipos = false;
      $scope.crearEmparejamientos = true;
      // Llamamos a una función para reordenar la nueva lista de equipos que utilizaremos para los emparejamientos
      $scope.reordenarEquipos($scope.equipos);
    };

    // Reordenamos la lista de los equipos para que vayan de forma aleatoria
    $scope.reordenarEquipos = function(equipos) {
      $scope.total=equipos.length;
        $scope.reordenar = [];
        
        for (var index = 0; index < $scope.total; index++) {
          $scope.aleatorio = Math.floor(Math.random()*(equipos.length));
          $scope.seleccion = equipos[$scope.aleatorio];
          $scope.reordenar.push($scope.seleccion);
          equipos.splice($scope.aleatorio, 1);
        }
        $scope.crearGrupos($scope.reordenar);
    };

    //Creamos los grupos del campeonato
    $scope.crearGrupos = function (equipos) {
      $scope.partidos = [];
      /*
      * Control y creación de los grupos del campeonato
      */
      $scope.letra = "abcd";
      if((equipos.length === 4) || (equipos.length===6) || (equipos.length===10) || (equipos.length===14)) {
        $scope.grupos = 2;
      }
      if((equipos.length === 8) || (equipos.length===12) || (equipos.length===16)) {
        $scope.grupos = 4;
      }
      $scope.divisionEquipos = equipos.length / $scope.grupos;
      for (var index = 0; index < $scope.grupos; index++) {
        $scope.partidos.push({grupo: "Grupo " + $scope.letra.substr(index , 1), equipos: []});
        $scope.sumaIndex = index + 1;
        for (var k = $scope.divisionEquipos * index; k < $scope.divisionEquipos * $scope.sumaIndex; k++) {
          $scope.partidos[index].equipos.push({id: equipos[k].id, equipo : equipos[k].name});
        }
      }
		$scope.crearEmparejamientosGrupos ($scope.partidos);
    };

	$scope.crearEmparejamientosGrupos = function (emparejamientos) {
		$scope.emparejamientosGrupos = [];
		//for para pasar por los grupos A, B, C, D
		for (var index = 0; index < emparejamientos.length; index++) {
			$scope.emparejamientosGrupos.push({grupo : emparejamientos[index].grupo, enfrentamientos : []});

      switch(emparejamientos[index].equipos.length) {
        // Caso para torneos de 4 y de 8
        case 2:
          $scope.equiposlength = 1;
          break;
        // Caso para torneos de 6 y de 12
        case 3:
          $scope.equiposlength = 3;
          break;
        // Caso para torneos de 16
        case 4:
          $scope.equiposlength = 6;
          break;
        // Caso para tornetos de 10
        case 5:
          $scope.equiposlength = 10;
          break;
        // Caso para tornetos de 14
        case 7:
          $scope.equiposlength = 21;
          break;
      }
			//for para crear los enfrentamientos
			for (var j = 0; j < $scope.equiposlength; j++) {
        console.log("Crea el partido número " + parseInt(j+1));
        $scope.identpartido = "partido"+index+j;
        $scope.emparejamientosGrupos[index].enfrentamientos.push({id: $scope.identpartido, equipo1: "", equipo2: "", resultado1: "_", resultado2: "_"});
        if($scope.equiposlength === 1) {
          $scope.crearPartidos1(index, j, emparejamientos, $scope.emparejamientosGrupos);
        }
        if($scope.equiposlength === 3) {
          $scope.crearPartidos3(index, j, emparejamientos, $scope.emparejamientosGrupos, $scope.equiposlength);
        }
        if($scope.equiposlength === 6) {
          $scope.crearPartidos6(index, j, emparejamientos, $scope.emparejamientosGrupos, $scope.equiposlength);
        }
			}
		}
	};

  $scope.crearPartidos1 = function (index, j, emparejamientos, emparejamientosGrupos) {
    for (var i = 0; i < emparejamientosGrupos[index].enfrentamientos.length; i++) {
      emparejamientosGrupos[index].enfrentamientos[j].equipo1 = emparejamientos[index].equipos[i].equipo;
      for(var h = i ; h < emparejamientosGrupos[index].enfrentamientos.length; h++ ) {
        emparejamientosGrupos[index].enfrentamientos[j].equipo2 = emparejamientos[index].equipos[parseInt(h+1)].equipo;
      }
    }
      //console.log(JSON.stringify(emparejamientosGrupos));
  };

  $scope.crearPartidos3 = function (index, j, emparejamientos, emparejamientosGrupos, versus) {
    for (var i = 0; i < emparejamientosGrupos[index].enfrentamientos.length; i++) {
      emparejamientosGrupos[index].enfrentamientos[j].equipo1 = emparejamientos[index].equipos[i].equipo;
      for(var h = i ; h < emparejamientosGrupos[index].enfrentamientos.length; h++ ) {
        if((parseInt(h+1) < versus) && (emparejamientos[index].equipos[i].equipo !== emparejamientos[index].equipos[parseInt(h+1)].equipo)) {
          emparejamientosGrupos[index].enfrentamientos[j].equipo2 = emparejamientos[index].equipos[parseInt(h+1)].equipo;
        } else {
          emparejamientosGrupos[index].enfrentamientos[j].equipo2 = emparejamientos[index].equipos[0].equipo;
        }
      }
    }
      //console.log(JSON.stringify(emparejamientosGrupos));
  };

  $scope.crearPartidos6 = function (index, j, emparejamientos, emparejamientosGrupos, versus) {
    console.log("j es : " + j)
        if(j < 4) {
          emparejamientosGrupos[index].enfrentamientos[j].equipo1 = emparejamientos[index].equipos[j].equipo;
        } else {
          emparejamientosGrupos[index].enfrentamientos[j].equipo1 = emparejamientos[index].equipos[j-4].equipo;
        }
        if(parseInt(j+1) < 4) {
          emparejamientosGrupos[index].enfrentamientos[j].equipo2 = emparejamientos[index].equipos[j+1].equipo;
        } else {
          emparejamientosGrupos[index].enfrentamientos[j].equipo2 = emparejamientos[index].equipos[j-2].equipo;
        }
        if(j=== 5) {
          emparejamientosGrupos[index].enfrentamientos[j].equipo1 = emparejamientos[index].equipos[j-2].equipo;
          emparejamientosGrupos[index].enfrentamientos[j].equipo2 = emparejamientos[index].equipos[0].equipo;
        }

      //console.log(JSON.stringify(emparejamientosGrupos));
  };

  }]);
