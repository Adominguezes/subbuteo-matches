'use strict';

/**
 * @ngdoc overview
 * @name subbuteoMatchesApp
 * @description
 * # subbuteoMatchesApp
 *
 * Main module of the application.
 */
angular
  .module('subbuteoMatchesApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/nuevo-torneo', {
        templateUrl: 'views/nuevo-torneo.html',
        controller: 'NuevoTorneoCtrl',
        controllerAs: 'nuevoTorneo'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
